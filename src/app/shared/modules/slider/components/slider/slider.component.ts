import {Component, Input, OnInit} from '@angular/core';
import { Slide } from '../../../../../models/slide';
import { repeat, skip, Subject, takeUntil, tap, timer} from 'rxjs';
import { SliderService } from '../../services/slider.service';
import { BaseDestroyComponent } from '../../../../components/base-destroy.component';
import { SliderStorageService } from '../../services/slider-storage.service';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
  providers: [
    SliderService,
    SliderStorageService
  ]
})
export class SliderComponent extends BaseDestroyComponent implements OnInit {
  @Input() timeOut: number = 5000;
  @Input() countSlides!: number;

  @Input() set slides(value: Slide[] | null) {
    this._slides = value || [];

    if (this._slides && this._slides.length) {
      this.sliceSlidesByCountSlides();
      this.sortSlidesByPriority();
      this.startSlideShow();
    }
  }
  get slides(): Slide[] {
    return this._slides;
  }

  public currentIndex: number = 0;

  private _slides: Slide[] = [];
  private readonly _start$ = new Subject<void>();

  constructor(private sliderService: SliderService) {
    super();
  }

  ngOnInit() {
    this.initSubscribeCurrentSlideIndex();
  }

  private initSubscribeCurrentSlideIndex(): void {
    this.sliderService.getCurrentSlideIndex$().pipe(takeUntil(this.unsubscribe$))
      .subscribe((index) => {
        this.updateCurrentSlideIndex(index);
      });
  }

  sortSlidesByPriority(): void {
    this._slides = this.slides?.sort((a, b) => a.priority - b.priority) || [];
  }

  sliceSlidesByCountSlides(): void {
    if (this.countSlides) {
      this._slides = this.slides?.splice(0, this.countSlides);
    }
  }

  startSlideShow(): void {
    timer(0, this.timeOut).pipe(
      takeUntil(this.unsubscribe$),
      skip(1),
      tap(() => this.sliderService.calculateNextSlideIndex(this.slides || [])),
      repeat({delay: () => this._start$}),
    ).subscribe();
  }

  prevSlide(): void {
    if (this.slides) {
      this.sliderService.setPrevSlideIndex(this.slides);
      this.resetSliderTimeOut();
    }
  }

  nextSlide(): void {
    if (this.slides) {
      this.sliderService.setNextSlideIndex(this.slides);
      this.resetSliderTimeOut();
    }
  }

  public resetSliderTimeOut(): void {
    this._start$.next();
  }

  private updateCurrentSlideIndex(index: number): void {
    this.currentIndex = index;
  }
}
