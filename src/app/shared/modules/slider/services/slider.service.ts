import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { Slide } from '../../../../models/slide';
import { SliderStorageService } from './slider-storage.service';

@Injectable()
export class SliderService {
  private _currentSlideIndex: BehaviorSubject<number> = new BehaviorSubject<number>(this.sliderStorage.storageValue);

  constructor(private sliderStorage: SliderStorageService) {
  }

  public getCurrentSlideIndex$(): Observable<number> {
    return this._currentSlideIndex.asObservable();
  }

  get currentSlideIndex(): number {
    return this._currentSlideIndex.value;
  }

  set currentSlideIndex(value:  number) {
    this.sliderStorage.storageValue = value;
    this._currentSlideIndex.next(value);
  }

  public calculateNextSlideIndex(slides: Slide[]): void {
    this.currentSlideIndex = (this.currentSlideIndex + 1) % slides.length;
  }

  public setPrevSlideIndex(slides: Slide[]): void {
    if (this.currentSlideIndex === 0) {
      this.currentSlideIndex = slides.length - 1;
    } else {
      this.currentSlideIndex = this.currentSlideIndex - 1;
    }
  }

  public setNextSlideIndex(slides: Slide[]): void {
    if (this.currentSlideIndex === (slides.length - 1)) {
      this.currentSlideIndex = 0;
    } else {
      this.currentSlideIndex = this.currentSlideIndex + 1;
    }
  }
}
