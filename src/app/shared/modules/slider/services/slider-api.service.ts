import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { Slide } from '../../../../models/slide';
import { MockData } from '../../../../models/mock';

@Injectable({
  providedIn: 'root'
})
export class SliderApiService {
  constructor(private http: HttpClient) {
  }

  public getSlider(): Observable<Slide[]> {
    return this.http.get<Slide[]>(`api/v1/slider`);
  }

  public getMockSlider(): Observable<Slide[]> {
    return of(MockData.MockSlider);
  }
}
