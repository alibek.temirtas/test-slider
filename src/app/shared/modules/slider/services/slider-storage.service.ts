import { Injectable } from '@angular/core';

@Injectable()
export class SliderStorageService {
  private STORAGE_KEY = 'SLIDER_STORAGE_CURRENT_INDEX';
  constructor() {
  }

  set storageValue(value: number) {
    localStorage.setItem(this.STORAGE_KEY, value.toString());
  }

  get storageValue(): number {
    return Number(localStorage.getItem(this.STORAGE_KEY)) || 0;
  }
}
