import { Component } from '@angular/core';
import { SliderApiService } from './shared/modules/slider/services/slider-api.service';
import { Observable } from 'rxjs';
import { Slide } from './models/slide';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public slides$: Observable<Slide[]>;

  constructor(private sliderApiService: SliderApiService) {
    this.slides$ = this.sliderApiService.getMockSlider();
  }
}
