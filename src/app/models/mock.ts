import { Slide } from './slide';

export class MockData {
  static AssetsImgSrc = '/assets/images/';

  static get MockSlider(): Slide[] {
    return [
      new Slide({
        link: 'https://ru.wikipedia.org/wiki/Mortal_Kombat',
        imgUrl: this.AssetsImgSrc + 'slider/slide_1.jpg',
        priority: 5
      }),
      new Slide({
        link: 'https://ru.wikipedia.org/wiki/Mortal_Kombat',
        imgUrl: this.AssetsImgSrc + 'slider/slide_2.jpg',
        priority: 2
      }),
      new Slide({
        link: 'https://ru.wikipedia.org/wiki/Mortal_Kombat',
        imgUrl: this.AssetsImgSrc + 'slider/slide_3.jpg',
        priority: 3
      }),
      new Slide({
        link: 'https://ru.wikipedia.org/wiki/Mortal_Kombat',
        imgUrl: this.AssetsImgSrc + 'slider/slide_4.jpg',
        priority: 4
      }),
      new Slide({
        link: 'https://ru.wikipedia.org/wiki/Mortal_Kombat',
        imgUrl: this.AssetsImgSrc + 'slider/slide_5.jpg',
        priority: 1
      })
    ];
  }
}
