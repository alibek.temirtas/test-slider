
export class Slide {
  id?: number;
  link: string;
  imgUrl: string;
  priority: number;

  constructor(opt: {
    id?: number,
    link: string,
    imgUrl: string,
    priority: number
  }) {
    this.id = Math.floor(Math.random() * 10);
    this.link = opt.link;
    this.imgUrl = opt.imgUrl;
    this.priority = opt.priority || 0;
  }
}
